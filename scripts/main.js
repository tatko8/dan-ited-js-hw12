"use strict"; //strict mode is a way to introduce better error-checking into your code 

// Питання. Чому для роботи з input не рекомендується використовувати клавіатуру?

// Віддповідь. дані можуть бути введені користувачем не тільки з використанням клавіатури, а й іншим чином, наприклад за допомогою миші (копіювати/вставити)
//             або на мобільних пристроях у інший спосіб, тому треба використовувати події самого input для відстеження змін у полі.

let arr = ["enter", "s", "e", "o", "n", "l", "z"];
let btn = document.querySelectorAll(".btn"); 

function color(e) {
    let character = e.key.toLowerCase();
        // console.log(character);
    let btnIndex = arr.indexOf(character);
        // console.log(btnIndex);

    if (btnIndex !== -1){
        btn.forEach((button) => (button.style.backgroundColor = "black"));
        btn[btnIndex].style.backgroundColor = "blue";
    }
};

document.addEventListener("keydown", color);
